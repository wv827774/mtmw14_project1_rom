# MTMW14_project1_ROM

The project 1 ipython notebook is the main code file, run this to produce graphs for tasks A-G.

- ROM_dictionaries contains parameters for all model runs.
- ROM_solvers contains functions used for the Runga-Kutta time schemes
- ROM_mainCde_backup is a backup main code file (also produces all graphs for all tasks) incase the ipython notebook file does not work



