"""
Contains all dictionary files to be used for tasks 1-G for the ROM project 1
"""

# =============================================================================
# TASK A
# =============================================================================

setup_RungaKuttaA = {'nt' : 1*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 2/3,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task A time series',
         'phasediagram_name' : 'Task A phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Running Task A for longer time period (100 oscillations)

setup_RungaKuttaA_ManyOscillations = {'nt' : 100*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 2/3,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task A time series - Many oscillations',
         'phasediagram_name' : 'Task A phase diagram - Many oscillations',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# =============================================================================
# TASK B
# =============================================================================

# # Control run, for 5 periods
setup_RungaKuttaB_exact = {'nt' : 41*5,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 2/3,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task B_exact time series',
         'phasediagram_name' : 'Task B_exact phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Coupling parameter lower than the critical value
setup_RungaKuttaB_above = {'nt' : 41*5,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 0.7,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task B_above time series',
         'phasediagram_name' : 'Task B_above phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Coupling parameter greater than the critical value
setup_RungaKuttaB_below = {'nt' : 41*5,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 0.6,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task B_below time series',
         'phasediagram_name' : 'Task B_below phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# =============================================================================
# TASK C 
# =============================================================================

# Control run, for 5 periods 
setup_RungaKuttaC_linear = {'nt' : 5*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 2/3,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task C_linear time series',
         'phasediagram_name' : 'Task C_linear phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Introducing non-linearity (non linear e_n)

setup_RungaKuttaC_NONlinear = {'nt' : 5*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 2/3,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task C_NONlinear time series',
         'phasediagram_name' : 'Task C_NONlinear phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Introducing non-linearity (non linear e_n), with a coupling parameter greater than
# the critical value
setup_RungaKuttaC_NONlinear_above = {'nt' : 5*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 0.7,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months)
         'Timeseries_name' : 'Task C_NONlinear_above time series',
         'phasediagram_name' : 'Task C_NONlinear_above phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# Introducing non-linearity for Mu greather than the critical value, for a long model runtime
setup_RungaKuttaC_NONlinear_above_ManyOscillations = {'nt' : 50*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu' : 0.7,      # Coupling coefficient - set to critical value for now
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,      # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months)
         'Timeseries_name' : 'Task C_NONlinear_above time series',
         'phasediagram_name' : 'Task C_NONlinear_above phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'off', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         
         }

# =============================================================================
# TASK D
# =============================================================================

# Control run, for 5 periods 
setup_RungaKuttaD = {'nt' : 5*41,     # Number of timesteps
         'dt' : 1,        # Timestep  (non-dimensionalised)
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient 
         'Tau' : 12,     # Constant of 12 months- to vary coupling coefficient
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,     # Varies degree of non-linearity of ROM
         'psi_1' : 0.,   # Represents random wind stress forcing added on system
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task D time series',
         'phasediagram_name' : 'Task D phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'off', # Determines whether random wind stress forcing is turned on/off
         }

# =============================================================================
# TASK E
# =============================================================================

setup_RungaKuttaE = {'nt' : 5*30*41,     # Number of timesteps
         'dt' : 1/30,     # Timestep in DAYS
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient
         'f_ann'  : 0.02,  # Constant for random wind stress forcing
         'f_ran'  : 0.2, # Constant for random wind stress forcing
         'Tau' : 12,      # Constant of 12 months- to vary coupling coefficient annually
         'Tau_cor' : 1/30, # Correlation timescale (in DAYS) for random wind stress forcing
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.,     # Varies degree of non-linearity of ROM
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task E time series',
         'phasediagram_name' : 'Task E phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'on', # Determines whether random wind stress forcing is turned on/off
         }

# =============================================================================
# TASK F
# =============================================================================

setup_RungaKuttaF = {'nt' : 5*30*41,     # Number of timesteps
         'dt' : 1/30,     # Timestep in DAYS
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient
         'f_ann'  : 0.02,  # Constant for random wind stress forcing
         'f_ran'  : 0.2, # Constant for random wind stress forcing
         'Tau' : 12,      # Constant of 12 months- to vary coupling coefficient annually
         'Tau_cor' : 1/30, # Correlation timescale (in DAYS) for random wind stress forcing
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,     # Varies degree of non-linearity of ROM
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task F time series',
         'phasediagram_name' : 'Task F phase diagram',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'on',} # Determines whether random wind stress forcing is turned on/off

# Running Task F with only annual variability in wind forcing (no random forcing)

setup_RungaKuttaF_annualOnly = {'nt' : 5*30*41,     # Number of timesteps
         'dt' : 1/30,     # Timestep in DAYS
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient
         'f_ann'  : 0.02,  # Constant for random wind stress forcing
         'f_ran'  : 0.2, # Constant for random wind stress forcing
         'Tau' : 12,      # Constant of 12 months- to vary coupling coefficient annually
         'Tau_cor' : 1/30, # Correlation timescale (in DAYS) for random wind stress forcing
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,     # Varies degree of non-linearity of ROM
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'Timeseries_name' : 'Task F time series - annual wind forcing only',
         'phasediagram_name' : 'Task F phase diagram - annual wind forcing only',
         'ensemble': 'off', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'annual', # Determines whether random wind stress forcing is turned on/off
         }

# =============================================================================
# TASK G
# =============================================================================

# Control run, for 5 periods 
setup_RungaKuttaG_withRandom = {'nt' : 5*30*41,     # Number of timesteps
         'dt' : 1/30,     # Timestep in DAYS
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient
         'f_ann'  : 0.02,  # Constant for random wind stress forcing
         'f_ran'  : 0.2, # Constant for random wind stress forcing
         'Tau' : 12,      # Constant of 12 months- to vary coupling coefficient annually
         'Tau_cor' : 1/30, # Correlation timescale (in DAYS) for random wind stress forcing
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,     # Varies degree of non-linearity of ROM
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (kelvin)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'ensemble': 'on', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/of
         'wind_forcing' : 'on', # Turning on random wind stress forcing for this model run
         'Timeseries_name' : 'Task G time series with random wind forcing',
         'phasediagram_name' : 'Task G phase diagram with random wind forcing'
         }

setup_RungaKuttaG_withoutRandom = {'nt' : 5*30*41,     # Number of timesteps
         'dt' : 1/30,     # Timestep in MONTHS - used to simulate DAILY timestep
         'Mu_0' : 0.75,   # Coupling coefficient constant - to vary coupling coefficient 
         'Mu_ann' : 0.2,  # Coupling coefficient constant - to vary coupling coefficient
         'f_ann'  : 0.02,  # Constant for random wind stress forcing
         'f_ran'  : 0.2, # Constant for random wind stress forcing
         'Tau' : 12,      # Constant of 12 months- to vary coupling coefficient annually
         'Tau_cor' : 1/30, # Correlation timescale (in MONTHS) for random wind stress forcing
         'b_0' : 2.5,     # High end value of coupling parameter
         'gamma' : 0.75,  # Specifies feedback of thermocline gradient on SST
         'c' : 1.,        # Damping rate of SST anomalies
         'r' : 0.25,      # Represents damping of upper ocean heat content (OHC)
         'alpha' : 0.125, # Relates enhanced easterly wind stress to upper OHC
         'e_n' : 0.1,     # Varies degree of non-linearity of ROM
         'psi_1' : 0,
         'psi_2' : 0.,   # Represents random heat added to system
         's_T'    : 7.5,  # Temperature scaling (°C)
         's_h'    : 150., # Thermocline depth scaling (metres)
         's_t'    : 2.,   # Time scaling (months?)
         'ensemble': 'on', # Determines whether ensemble run is to be performed
         'Mu_annualVariability' : 'on', # Determines whether annual variability in Mu is on/off
         'wind_forcing' : 'annual', # Determines whether random wind stress forcing is turned on/off
         'Timeseries_name' : 'Task G time series without random wind forcing',
         'phasediagram_name' : 'Task G phase diagram without random wind forcing'
         }