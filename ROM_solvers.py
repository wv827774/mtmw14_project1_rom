# -*- coding: utf-8 -*-
"""
Defining functions to compute Temperature anomaly and thermocline depth
"""

def depth_func(h,T,time,b,r,alpha,psi_1):
    """ Computes dh/dt (change in western Pacific thermocline depth 
     with time) for the ENSO recharge oscillator using coupling parameter, Mu,
as a function of time, t, with random wind stress forcing as a function of W"""     

    return -r*h - alpha*b*T - alpha*psi_1

def Temp_func(h,T,time,b,R,e_n,gamma,psi_1,psi_2):
    """ Computes dT/dt (change in Eastern Pacific sea surface temperature with time)
     for the ENSO recharge oscillator using coupling parameter, Mu,
as a function of time, t, with random wind stress forcing as a function of W"""
    
    return R*T + gamma*h - e_n*(h + b*T)**3 + gamma*psi_1 + psi_2

  # time_series = np.arange(0, nt*dt + dt, dt)
  # print(time_series)
  
  
def ROM_RungaKutta(setup):
    """Runga-Kutta time scheme to model the ENSO recharge oscillator mode 
    (Jin et al., 1997) """
    
    import numpy as np
    import matplotlib.pyplot as plt
    import sys
# =============================================================================
# EXTRACTING PARAMETERS
# =============================================================================
    
    
    # Flag defining whether an annual cycle in the coupling parameter is to be intrdoduced
    Mu_annualVariability = setup['Mu_annualVariability']
    # Flag defining whether nature of wind forcing (psi_1) to be applied
    wind_forcing = setup['wind_forcing']
    # Flag defining whether ensemble is to be performed
    ensemble = setup['ensemble'] 
    
    # Extracting non-dimensional scaling parameters
    s_T = setup['s_T']
    s_h = setup['s_h']
    s_t = setup['s_t']
    
    # Extracting model parameters from dictionary (non-task dependant -
    # see dictionaries for definitons of variables)
    nt = setup['nt']
    dt = setup['dt']/s_t     # Scaling time (with units of months)
    r = setup['r']
    alpha = setup['alpha']
    gamma = setup['gamma']
    b_0 = setup['b_0']
    c = setup['c']
    
    # Extracting coupling coefficient parameters (subject to whether annual variation exists)
    if Mu_annualVariability == 'on':
        Mu_0 = setup['Mu_0']
        Mu_ann = setup['Mu_ann']
        Tau = setup['Tau']/s_t   # Scaling Tau to give same units as time 't'
    else: 
        Mu = setup['Mu']
        b = b_0*Mu  
        R = gamma*b - c 
        
    # Extracting wind forcing parameters - subject to whether wind forcing is to be included
    if  wind_forcing == 'on' or wind_forcing ==  'annual':
        f_ann = setup['f_ann']
        f_ran = setup['f_ran']
        Tau = setup['Tau']/s_t
        Tau_cor = setup['Tau_cor']/s_t

    elif wind_forcing == 'off':
        psi_1 = setup['psi_1']

        
    # Extracting non-linear and external forcing parameters
    e_n = setup['e_n']
    psi_2 = setup['psi_2']


# =============================================================================
# DEFINING ARRAYS AND INITIAL CONDITIONS
# =============================================================================
    
    # Defining number of ensemble members to test sensitivity to initial conditions 
    # (if applicable)
    if ensemble == 'on':
       nm = 12
    else:
       nm = 1

    # Defining 2D arrays for NON-DIMENSIONAL Eastern Pacific temperature anomaly T,
    # and West Pacific thermocline depth, h
    T = np.zeros((nt+1,nm))
    h = np.zeros((nt+1,nm))

# =============================================================================
# SOLVING USING A RUNGA-KUTTA TIME SCHEME
# =============================================================================

   # Looping over all ensemble members, each with a random perturbation to initial conditions
   # (if applicable)
    for im in range(nm):
        # Defining perturbations to initial conditions if ensemble is to be performed
        if ensemble == 'off':
            T[0,im] = 1.125/s_T
            h[0,im] = 0/s_h
        else:
            T[0,im] = 1.125/s_T + np.random.normal(0/s_T,1/3/s_T) # Initial temperature anomaly 
            h[0,im] = 0/s_h  + np.random.normal(0/s_h,1/3/s_h)    # Initial thermocline depth
        
        # Ensuring thermocline depth cannot be negative
        if h[0,im] < sys.float_info.epsilon:
                h[0,im] = 0
        
    # Solving for thermocline depth, h, and # temperature anomaly T, in time, 
    # using a Runge-Kutta scheme.
        for i in range(nt):
            
            # Defining point in time for calulation of coupling coefficient Mu
            time = i*dt                    
            
            # Defining annual variation in coupling coefficient, Mu, if applicable.
            if Mu_annualVariability == 'on':
               Mu = Mu = Mu_0*(1 + Mu_ann*np.cos(2*np.pi*time/Tau - 5*np.pi/6)) 
               # Using coupling coeeficient Mu to compute b (measure of thermocline slope)
               # and R (term describing the Bjerknes positive feedback) if applicable.
               b = b_0*Mu  
               R = gamma*b - c         
            
            # Defining a wind stress forcing with annual and random components if applicable.
            # all wind forcing is turned off if wind forcing  is turned to 'off' 
            # (as defined above). 'annual' leaves only annual variability in wind forcing.
            # 'on' provides both annual and random variability to the wind forcing 
            # (see setup dictionary).
            if wind_forcing == 'on':
                W = np.random.normal(0,1/3)
                psi_1 = f_ann*np.cos(2*np.pi*time/Tau) + f_ran*W*Tau_cor/dt
            elif wind_forcing == 'annual':
                psi_1 = f_ann*np.cos(2*np.pi*time/Tau)
            #else:
               # psi_1 = 0
                
            # Runga-Kutta scheme
            hk1 = depth_func(h[i,im], T[i,im],time,b,r,alpha,psi_1)
            Tk1 = Temp_func(h[i,im], T[i,im],time,b,R,e_n,gamma,psi_1,psi_2)
            
            hk2 = depth_func(h[i,im] + hk1*dt/2, T[i,im] + Tk1*dt/2,time,b,r,alpha,psi_1)
            Tk2 = Temp_func(h[i,im] + hk1*dt/2, T[i,im] + Tk1*dt/2,time,b,R,e_n,gamma,psi_1,psi_2)
            
            hk3 = depth_func(h[i,im] + hk2*dt/2, T[i,im] + Tk2*dt/2,time,b,r,alpha,psi_1)
            Tk3 = Temp_func(h[i,im] + hk2*dt/2, T[i,im] + Tk2*dt/2,time,b,R,e_n,gamma,psi_1,psi_2)
            
            hk4 = depth_func(h[i,im] + hk3*dt, T[i,im] + Tk3*dt,time,b,r,alpha,psi_1)
            Tk4 = Temp_func(h[i,im] + hk3*dt, T[i,im] + Tk3*dt,time,b,R,e_n,gamma,psi_1,psi_2)
        
            h[i+1,im] = h[i,im] + dt*1/6*(hk1 + 2*hk2 + 2*hk3 + hk4)
            T[i+1,im] = T[i,im] + dt*1/6*(Tk1 + 2*Tk2 + 2*Tk3 + Tk4)
    
    
    # Plotting time series of temperature anomaly, T, and thermocline depth, h 
    fig,ax = plt.subplots()
    ax.plot
    ax.plot(np.arange(0, nt*dt + dt, dt)*s_t,T*s_T, color = 'red', label = 'Temp anomaly (°C)')
    ax.plot(np.arange(0, nt*dt + dt, dt)*s_t,h*s_h, color = 'blue', label = 'Thermocline depth (m)')
    plt.xlabel('Time (months)')
    plt.ylabel('values')
    plt.savefig(setup['Timeseries_name'])
    ax.legend
    
    # Plotting a phase diagram of redimensionalised Temp anomaly, T, 
    # against thermocline depth, h
    fig,ax = plt.subplots()
    ax.plot(T*s_T,h*s_h, label = 'Temp anomaly', zorder=1)
    # Plotting ensemble endpoints if ensemble used
    if ensemble == 'on':
        Ensemble_endpoints_T = T[nt,:]
        Ensemble_endpoints_h = h[nt,:]
        plt.scatter(Ensemble_endpoints_T*s_T,Ensemble_endpoints_h*s_h, color='red', zorder=2)
    plt.xlabel('Temp anomaly (°C)')
    plt.ylabel('Thermocline depth (m)')
    plt.show()
    plt.savefig(setup['phasediagram_name'])
    ax.legend
