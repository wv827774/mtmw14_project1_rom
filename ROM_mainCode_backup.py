"""
Main code file to run all code for project 1
"""

from ROM_dictionaries import *
from ROM_solvers import *

# Task A
ROM_RungaKutta(setup_RungaKuttaA)
ROM_RungaKutta(setup_RungaKuttaA_ManyOscillations)

# Task B
ROM_RungaKutta(setup_RungaKuttaB_exact)
ROM_RungaKutta(setup_RungaKuttaB_above)
ROM_RungaKutta(setup_RungaKuttaB_below)

# Task C
ROM_RungaKutta(setup_RungaKuttaC_linear )
ROM_RungaKutta(setup_RungaKuttaC_NONlinear)
ROM_RungaKutta(setup_RungaKuttaC_NONlinear_above)
ROM_RungaKutta(setup_RungaKuttaC_NONlinear_above_ManyOscillations)

# Task D
ROM_RungaKutta(setup_RungaKuttaD)

# Task E
ROM_RungaKutta(setup_RungaKuttaE)

# Task F
ROM_RungaKutta(setup_RungaKuttaF)
ROM_RungaKutta(setup_RungaKuttaF_annualOnly)

# Task G
ROM_RungaKutta(setup_RungaKuttaG_withRandom)
ROM_RungaKutta(setup_RungaKuttaG_withoutRandom)